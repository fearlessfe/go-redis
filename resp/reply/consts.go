package reply

// Pong reply
type PongReply struct {
}

var pongBytes = []byte("+PONG\r\n")

func (r *PongReply) ToBytes() []byte {
	return pongBytes
}

func MakePongReply() *PongReply {
	return &PongReply{}
}

// OK reply
type OkReply struct {
}

var okBytes = []byte("+OK\r\n")

func (r *OkReply) ToBytes() []byte {
	return okBytes
}

func MakeOKReply() *OkReply {
	return &OkReply{}
}

// Null reply
type NullBulkReply struct {
}

var nullBulkBytes = []byte("$-1\r\n")

func (r *NullBulkReply) ToBytes() []byte {
	return nullBulkBytes
}

func MakeNullBulkReply() *NullBulkReply {
	return &NullBulkReply{}
}

// Empty mutilBulk reply
type EmptyMutilBulkReply struct {
}

var emptyMutilBulkReply = []byte("*0\r\n")

func (r *EmptyMutilBulkReply) ToBytes() []byte {
	return emptyMutilBulkReply
}

func MakeEmptyMutilBulkReply() *EmptyMutilBulkReply {
	return &EmptyMutilBulkReply{}
}

// No reply
type NoReply struct {
}

var noReply = []byte("")

func (r *NoReply) ToBytes() []byte {
	return noReply
}

func MakeNoReply() *NoReply {
	return &NoReply{}
}
