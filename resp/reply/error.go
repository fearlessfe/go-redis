package reply

// UnknowErrReply
type UnknowErrReply struct {
}

var unknowErrBytes = []byte("-Err unknown\r\n")

func (u UnknowErrReply) Error() string {
	return "Err unknown"
}

func (u UnknowErrReply) ToBytes() []byte {
	return unknowErrBytes
}

// ArgNumErrReply
type ArgNumErrReply struct {
	Cmd string
}

func (r *ArgNumErrReply) Error() string {
	return "-Err wrong number of arguments for '" + r.Cmd + "' command"
}

func (r *ArgNumErrReply) ToBytes() []byte {
	return []byte("-Err wrong number of arguments for '" + r.Cmd + "' command\r\n")
}

func MakeArgNumErrReply(cmd string) *ArgNumErrReply {
	return &ArgNumErrReply{
		Cmd: cmd,
	}
}

// SyntaxErrReply
type SyntaxErrReply struct {
}

var syntaxErrBytes = []byte("-Err syntax error\r\n")

func (u SyntaxErrReply) Error() string {
	return "Err unknown"
}

func (u SyntaxErrReply) ToBytes() []byte {
	return syntaxErrBytes
}

func MakeSyntaxErrReply(cmd string) *SyntaxErrReply {
	return &SyntaxErrReply{}
}

// WrongTypeErrReply
type WrongTypeErrReply struct {
}

var wrongTypeErrBytes = []byte("-WRONGTYPE Operation against a key holding the wrong kind of value\r\n")

func (u WrongTypeErrReply) Error() string {
	return "WRONGTYPE Operation against a key holding the wrong kind of value"
}

func (u WrongTypeErrReply) ToBytes() []byte {
	return wrongTypeErrBytes
}

func MakeWrongTypeErrReply(cmd string) *WrongTypeErrReply {
	return &WrongTypeErrReply{}
}

// ProtocolErrReply
type ProtocolErrReply struct {
	Msg string
}

func (u ProtocolErrReply) Error() string {
	return "-ERR Protocol error:" + u.Msg
}

func (u ProtocolErrReply) ToBytes() []byte {
	return []byte("-ERR Protocol error:'" + u.Msg + "'\r\n")
}

func MakeProtocolErrReply(msg string) *ProtocolErrReply {
	return &ProtocolErrReply{
		Msg: msg,
	}
}
