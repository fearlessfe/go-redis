package reply

import (
	"bytes"
	"go-redis/interface/resp"
	"strconv"
)

type ErrorReply interface {
	Error() string
	ToBytes() []byte
}

var (
	nullBulkReplyBytes = []byte("$-1")
	CRLF               = "\r\n"
)

type BulkReply struct {
	Arg []byte
}

func (b *BulkReply) ToBytes() []byte {
	if len(b.Arg) == 0 {
		return nullBulkReplyBytes
	}
	return []byte("$" + strconv.Itoa(len(b.Arg)) + CRLF + string(b.Arg) + CRLF)
}

func MakeBulkReply(arg []byte) *BulkReply {
	return &BulkReply{
		Arg: arg,
	}
}

type MultiBulkReply struct {
	Args [][]byte
}

func (b *MultiBulkReply) ToBytes() []byte {

	argLen := len(b.Args)
	var buf bytes.Buffer
	buf.WriteString("*" + strconv.Itoa(argLen) + CRLF)

	for _, arg := range b.Args {
		l := len(arg)
		if arg == nil {
			buf.WriteString(string(nullBulkReplyBytes) + CRLF)
		} else {
			buf.WriteString("$" + strconv.Itoa(l) + CRLF + string(arg) + CRLF)
		}

	}

	return buf.Bytes()
}

func MakeMultiBulkReply(arg [][]byte) *MultiBulkReply {
	return &MultiBulkReply{
		Args: arg,
	}
}

type StatusReply struct {
	Status string
}

func (b *StatusReply) ToBytes() []byte {

	return []byte("+" + b.Status + CRLF)
}

func MakeStatusReply(status string) *StatusReply {
	return &StatusReply{
		Status: status,
	}
}

type IntReply struct {
	Code int64
}

func (b *IntReply) ToBytes() []byte {

	return []byte(":" + strconv.FormatInt(b.Code, 10) + CRLF)
}

func MakeIntReply(code int64) *IntReply {
	return &IntReply{
		Code: code,
	}
}

type StandardErrReply struct {
	Msg string
}

func (b *StandardErrReply) ToBytes() []byte {

	return []byte("-" + b.Msg + CRLF)
}

func MakeStandardErrReply(msg string) *StandardErrReply {
	return &StandardErrReply{
		Msg: msg,
	}
}

func IsErrReply(reply resp.Reply) bool {
	return reply.ToBytes()[0] == '-'
}
