package tcp

import (
	"fmt"
	"net"
	"os"
	"testing"
)

func TestServer(t *testing.T) {
	c, err := net.Dial("tcp", "127.0.0.1:6379")
	if err != nil {
		t.Fatal(err)
	}
	_, err = c.Write([]byte("12312321\n"))
	if err != nil {
		os.Exit(1)
	}
	received := make([]byte, 1024)

	_, _ = c.Read(received)
	fmt.Print(string(received))

}
